/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
    colors: {
      'off-white': 'hsl(225, 40%, 83%)',
      'lighter-grey': 'hsl(225, 21%, 45%)',
      'white': 'hsl(0, 0%, 100%)',
      'dark-grey': 'hsl(224, 35%, 11%)',
      'light-grey':'hsl(225, 26%, 23%)',
      'light-green':'hsl(157, 74%, 62%)',
      'dark-desaturated-blue': 'hsl(244, 38%, 16%)',
      'grey': 'hsla(224, 26%, 23%, 1)',
      'error': 'hsla(0, 96%, 61%, 1)',
      'active': 'hsla(157, 100%, 85%, 1)'
    },
    fontFamily: {
      Chivo: ['Chivo', 'cursive'],
    }
  },
  plugins: [],
}

